// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"
#include <iostream>
#include <ctime>

// Sets default values
AMyActor::AMyActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentTime = 0;
	UpDirection = true;
	Step = 100;
	TimeToFly = 1;

}

	void AMyActor::CheckTime()

	{

		FCollisionQueryParams Params;
		FHitResult Hit;


		double startTime = FPlatformTime::Seconds();

		for (float size = 0; size <= 1000000; size++)
		{
			GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + (GetActorLocation().GetSafeNormal() * 500), ECC_Visibility, Params);
		}

		double endTime = FPlatformTime::Seconds();
		double searchTime = endTime - startTime;

		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, FString::SanitizeFloat(searchTime));
	}






// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	AMyActor::CheckTime();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

	float DistanceToMove = Step * DeltaTime;

	if (UpDirection)
	{
		AddActorWorldOffset(FVector(0, 0, DistanceToMove));
		CurrentTime += DeltaTime;
		if (CurrentTime >= TimeToFly) { UpDirection = false; }
	}
	else
	{
		AddActorWorldOffset(FVector(0, 0, (-1)*DistanceToMove));
		CurrentTime -= DeltaTime;
		if (CurrentTime <= 0) { UpDirection = true; }
	}
	
}

