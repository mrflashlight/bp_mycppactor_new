// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class MR_FS_1STPR_ARTCRAFT_API AMyActor : public AActor
{
	GENERATED_BODY()

			
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void CheckTime();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	

public:
	double CurrentTime;
	bool UpDirection;
	int Step, TimeToFly;

};
